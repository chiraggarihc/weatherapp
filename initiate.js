const cityTemp = require('./CitywiseTemp.js');
const MonthTemp = require('./CitywiseAvgTempMonth.js');
const CityDatatoDb = require('./getHourlyData.js');

const express = require('express');
//var db = require('./hourlyWeatherData.js');
const app = express()
const port = 8080
let result = {};


/* API to get the current temperature of city */
app.get('/getcurrenttemp/:city', async (req, res) => {
  result = await cityTemp.getCurrentTemp(req.params.city);
  return res.send(result);

});

/* API to get the current temperature in Covilha*/
app.get('/currenttempincovilha', async (req, res) => {
  result = await cityTemp.getCurrentTemp("Covilha");
  return res.send(result);
});

/* API to get the average temperature of month in June*/
app.get('/avgtempinsfax/:month', async (req, res) => {
  result = await MonthTemp.getAvgTempMonth("sfax", req.params.month);
  return res.send(result);

});


/* API to get the average temperature of city in particular month*/
app.get('/avgtemp/:city/:month', async (req, res) => {
  result = await MonthTemp.getAvgTempMonth(req.params.city, req.params.month); 

  return res.send(result);

});

/* API to get the average temperature of city in particular month*/
app.get('/insertData/:city', async (req, res) => {
  result = await CityDatatoDb.insertWeatherData(req.params.city); 

  return res.send(result);

});

console.log(" Server Started " + process.env.DATABASEUSERNAME);

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));