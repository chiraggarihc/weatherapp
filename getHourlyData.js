"use strict";
const axios = require('axios');
var MongoClient = require('mongodb').MongoClient;
require('dotenv').config();
const MongoURL= 'mongodb://'+process.env.DATABASEUSERNAME+':'+process.env.DATABASEPASSWORD+'@'+process.env.DATABASEHOST+':'+process.env.DATABASEPORT;
const dbName = process.env.dbName;

let cachedDb = null;

async function insertWeatherData(city)
 {
        // console.log('LogScheduledEvent');
//    const city = "Sfax";
    const data = await getHourlyData(city);
     //console.log("get data: ");
     //console.log("data ", data.cod);
  
    let response;
    if (data.cod == '200') {
  
      // console.log("event: ", event);
  
      return connectToDatabase(MongoURL)
        .then(client => queryDatabase(client.db(dbName), data))
        .then(result => {
          // console.log("=> returning result: ", result);
          return result;
        })
        .catch(err => {
          console.log("=> an error occurred: ", err);
          return err;
        });
    }
    else{
      console.log("Error with API");
      return data.response.data;;
    }
}

async function getHourlyData(city) {
  try {
    const response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?appid=${process.env.OPENWEATHER_API_ID}&units=metric&q=${city}`);
    var date = new Date((response.data.dt * 1000));
    response.data.month = date.getMonth();
    //console.log(response.data);
    return response.data;
  }
  catch (error) {
    //console.error("error " , error);
    return error;
  }
}

function connectToDatabase(uri) {
  // console.log("=> connect to database");

  if (cachedDb) {
    // console.log("=> using cached database instance");

    return Promise.resolve(cachedDb);
  }

  return MongoClient.connect(uri).then(db => {
    cachedDb = db;

    return cachedDb;
  });
}

function queryDatabase(db, data) {
  // console.log("=> query database");

  return db.collection(process.env.DBCOLLECTION).insertOne(data).then(() => {
      console.log("1 document inserted");
      return { statusCode: 200, body: "success" };
    })
    .catch(err => {
      console.log("=> an error occurred: ", err);
      return { statusCode: 500, body: "error" };
    });
}

exports.getHourlyData = getHourlyData;
exports.queryDatabase = queryDatabase;
exports.insertWeatherData = insertWeatherData;
exports.connectToDatabase = connectToDatabase;
