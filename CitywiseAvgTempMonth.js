"use strict";
var MongoClient = require("mongodb").MongoClient;
require('dotenv').config();

const dbName = process.env.dbName;
const MongoURL= 'mongodb://'+process.env.DATABASEUSERNAME+':'+process.env.DATABASEPASSWORD+'@'+process.env.DATABASEHOST+':'+process.env.DATABASEPORT;

let cachedDb = null;
//var url = 'mongodb://localhost/';
//const dbName = 'WeatherApp';

async function getAvgTempMonth (city, month) {
	
	// console.log(event["city"]);
  
	return connectToDatabase(MongoURL)
	  .then(
		async client =>
		  await queryDatabase(client.db(dbName),city,month)
	  )
	  .then(result => {
		// console.log("=> returning result: ", result);
		console.log(result);
		return result;
		//callback(null, result);
	  })
	  .catch(err => {
		console.log("=> an error occurred: ", err);
		return error;
		//callback(err);
	  });
}


function connectToDatabase(uri) {
  // console.log("=> connect to database");

  if (cachedDb) {
    // console.log("=> using cached database instance");

    return Promise.resolve(cachedDb);
  }

  return MongoClient.connect(uri).then(db => {
    cachedDb = db;

    return cachedDb;
  });
}

function queryDatabase(db, city, month) {
  // console.log("=> query database");
  const monthIndex = new Date(
    Date.parse(month + " 1, 2020")
  ).getMonth();
  // console.log(monthIndex + "of city : " + city);
  /* check if monthindex is correct */
  if (Number.isNaN(monthIndex)) {
    console.log("Invalid Month request");
    // TODO: update return message
    return;
  }

  const query = [
    {
      $match: {
        $and: [
          { name: { $regex: new RegExp(city, "i") } },
          { month: { $eq: monthIndex } },
        ],
      },
    },
    {
      $group: {
        _id: null,
        AverageTemperature: { $avg: "$main.temp" },
        AverageTemperatureMin: { $avg: "$main.temp_min" },
        AverageTemperatureMax: { $avg: "$main.temp_max" },
      },
    },
  ];

  return db
    .collection(process.env.DBCOLLECTION)
    .aggregate(query)
    .toArray()
    .then(response => {
      return { statusCode: 200, body: "success", response };
    })
    .catch(err => {
      console.log("=> an error occurred: ", err);
      return { statusCode: 500, body: "error" };
    });
}


exports.queryDatabase = queryDatabase;

//exports.getAvgTempMonth = getAvgTempMonth;
exports.getAvgTempMonth = getAvgTempMonth;



