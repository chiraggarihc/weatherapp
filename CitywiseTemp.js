const axios = require('axios');
const e = require('express');
require('dotenv').config();
var getCurrentTemp = async function (city) {


	const result = {};
	try {
		/* API which gets current weather data of current API*/
		const response = await axios.get(
			`http://api.openweathermap.org/data/2.5/weather?appid=${process.env.OPENWEATHER_API_ID}&units=metric&q=${city}`
		);

		// 		/* API which gets current weather data of current API*/
		// 		const response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?appid=5f65c1e48bb58d067d2c1168b319b144&units=metric&q=${city}`);

		var JsonObj = response.data;

		/* Returning the data with required data and response message*/
		result["response"] = JsonObj.cod;
		result["message"] = "Success";
		result["Timestamp"] = new Date(JsonObj.dt * 1000); //Converting epoch time to Date object
		/* getting the data in metric system */
		result["units"] = "°C";
		result["Temp"] = JsonObj.main["temp"];
		result["Country"] = JsonObj.sys["country"];
		result["City"] = JsonObj.name;
		// console.log(`Temperature of ${city} =`, result);
		return result;
	} catch (error) {
		/* Error message parsing */
		console.log(
			`${error.response.data.message} error throw while getting current temperature`
		);
		return error.response.data;
	}
};

exports.getCurrentTemp = getCurrentTemp;