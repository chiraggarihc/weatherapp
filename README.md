# WeatherApp

The Project consist of Web application which provides API of weather/temperature of different cities.

## TechStack :
NodeJS, Express JS, MongoDB, AWS Lambda, AWS EC2, AWS API Gateway, AWS Cloud formation, AWS Event Bridge

## API Details :

## REST API :

Get current temperature of Covilha
----
  <_Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple)._>

* **URL**

  /currenttempincovilha

* **Method:**

    `GET`
*  **URL Params**

   <_ No Parameter Required._> 

* **Data Params**

  Not required

* **Success Response:**

    - **Code:** 200 <br />

    **Content:** `{"response":200,"message":"Success","Timestamp":"2020-07-01T14:32:03.000Z","units":"°C","Temp":26.11,"Country":"PT","City":"Covilha"}`
 

* **Error Response:**

  + **Code:** 404 <br />

    **Content:** `{ message: error throw while getting current temperature }`
* **Sample Call:**

 https://64ct8el8nh.execute-api.ap-south-1.amazonaws.com/Beta/currenttempincovilha

* **Notes:**

  <_This API will give the current temperature in the Covilha in Degree celsius with timestamp of server._> 

  REST API :

Get Current Temperature
----
  <_Additional information about your API call. Try to use verbs that match both request type (fetching vs modifying) and plurality (one vs multiple)._>

* **URL**

  /getcurrenttemp

* **Method:**

    `GET`
*  **URL Params**

  **Required:**
 
  `city=[string]`
   <_ City location required._> 

* **Data Params**

  Not required

* **Success Response:**

    - **Code:** 200 <br />

    **Content:** `{"response":200,"message":"Success","Timestamp":"2020-07-01T14:46:12.000Z","units":"°C","Temp":29,"Country":"IN","City":"Mumbai"}`
 

* **Error Response:**

  + **Code:** 404 <br />

    **Content:** `{"cod":"404","message":"city not found"}`
  + **Code:** 400 <br />

    **Content:** `{ message: Missing required request parameters: [city]}`
* **Sample Call:**

 https://64ct8el8nh.execute-api.ap-south-1.amazonaws.com/Beta/getcurrenttemp?city=Mumbai

* **Notes:**

  <_This API will give the current temperature of the selected City in Degree celsius with timestamp of server._> 

  REST API :

Insert Weather Data
----
  <_Inserts Weather Data of the city in to the Database of MongoDB._>

* **URL**

  /insertweatherdata

* **Method:**

    `GET`
*  **URL Params**

  **Required:**
 
  `city=[string]`
   <_ City location required._> 

* **Data Params**

  Not required

* **Success Response:**

    - **Code:** 200 <br />

    **Content:** `{"statusCode":200,"body":"success"}`
 

* **Error Response:**

  + **Code:** 500 <br />
    **Content:** `{"statusCode":500,"body":"error"}`

* **Sample Call:**

 https://64ct8el8nh.execute-api.ap-south-1.amazonaws.com/Beta/insertdata?city=Mumbai

* **Notes:**

  <_This API will get the current weather of selected city and store it in the database with month and timestamp._> 

 
  REST API :

Average Temperature Data
----
  <_Average Temperature of city and month will be shown with Temperature min and temperature Max._>

* **URL**

  /avgtemp

* **Method:**

    `GET`
*  **URL Params**

  **Required:**
 
  `city=[string]`
   <_ City location required._> 

   `Month=[string]`
   <_ Month required._> 

* **Data Params**

  Not required

* **Success Response:**

    - **Code:** 200 <br />

    **Content:** `{"statusCode":200,"body":"success","response":[{"_id":null,"AverageTemperature":29.095238095238095,"AverageTemperatureMin":29.095238095238095,"AverageTemperatureMax":29.095238095238095}]}`
 

* **Error Response:**

  + **Code:** 400 <br />
    **Content:** `{"message": "Missing required request parameters: [month, city]"}`

* **Sample Call:**

 https://64ct8el8nh.execute-api.ap-south-1.amazonaws.com/Beta/avgtemp?city=Sfax&month=June

* **Notes:**

  <_This API will get the average temperature of selected city and month available in the database, If the data is not stored it will return empty object._> 