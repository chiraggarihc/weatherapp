var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost/';
const dbName = 'WeatherApp';

async function insertWeatherData() {
    try {
        //const result = await apiFunctionWrapper("query all users");
        //console.log(result);
        /* store the data in the database  */
        MongoClient.connect(url, async function (err, client) {

            console.log("Connected successfully to server");
            const db = client.db(dbName);

            const collection = db.collection('CityWeather');

            collection.find({}).toArray(function (err, docs) {
                console.log("Found the following records");
                console.log(docs);
            });

            client.close();
        });

        // the next line will fail
        //const result2 = await apiFunctionWrapper("bad query");
    } catch (error) {
        console.error("ERROR:" + error);
    }
}

insertWeatherData();